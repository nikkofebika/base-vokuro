<?php
namespace Vokuro\Controllers;

/**
 * Display the default index page.
 */
class IndexController extends ControllerBase
{

    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function indexAction()
    {
        $this->view->setVar('logged_in', is_array($this->auth->getIdentity()));
        // $logged_in = is_array($this->auth->getIdentity());
        // $this->debug($logged_in);die;
        // if ($logged_in === 1) {
        // 	die('sudah login');
        // }else{
        // 	die('belum login');
        // }
        // echo "<pre>";
        // print_r($this->auth->getIdentity());
        // echo "</pre>";die;
        // $this->debug($this->auth->getIdentity());die;
        $this->view->setTemplateBefore('public');
    }

    function kmAction(){
    	$this->mail->send([
                'febika.nikko@gmail.com' => 'Nikko'
            ], "Please confirm your email", 'confirmation', [
                'confirmUrl' => '/confirm/123/febika.nikko@gmail.com'
            ]);
    }
}
